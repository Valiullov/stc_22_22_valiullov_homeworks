create table account (
                         id serial primary key,
                         first_name varchar(20),
                         last_name varchar(20),
                         phone_number varchar(11),
                         work_experience integer check ( work_experience >= 0 and work_experience < 100),
                         age integer check ( age >= 18 and age < 120),
                         is_driver bool,
                         driver_license varchar(2),
                         rating integer check ( rating >= 0 and rating <= 5)
);

create table automobile (
                            id serial primary key,
                            model  varchar(20),
                            color  varchar(20),
                            number varchar(11),
                            account_id integer not null,
                            foreign key (account_id) references account(id)
);

create table ride
(
    id serial primary key,
    account_id integer not null,
    foreign key (account_id) references account(id),
    automobile_id integer not null,
    foreign key (automobile_id) references automobile(id),
    ride_date     date,
    ride_time     interval
);
