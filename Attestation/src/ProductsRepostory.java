import java.util.List;

public interface ProductsRepostory {

    Product ProductfindById(Integer id);

    List<Product> findAllByTitleLike(String title);

}
