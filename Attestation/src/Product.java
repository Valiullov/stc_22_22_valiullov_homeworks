public class Product {
    private Integer id;
    private String product;
    private Float productValue;
    private int productAmount;

    public Product(Integer id, String product, Float productValue, int productAmount) {
        this.id = id;
        this.product = product;
        this.productValue = productValue;
        this.productAmount = productAmount;
    }

    public Integer getId() {
        return id;
    }

    public String getProduct() {
        return product;
    }

    public Float getProductValue() {
        return productValue;
    }

    public int getProductAmount() {
        return productAmount;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", product='" + product + '\'' +
                ", productValue=" + productValue +
                ", productAmount=" + productAmount +
                '}';
    }
}
