import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProductsRepositoryFileBasedImpl implements ProductsRepostory {

    private final String fileName;

    public ProductsRepositoryFileBasedImpl(String fileName) {

        this.fileName = fileName;
    }

    private static final Function<String, Product> stringToProductMapper = currentUser -> {
        String[] parts = currentUser.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String product = parts[1];
        Float productValue = Float.valueOf(parts[2]);
        Integer productAmount = Integer.parseInt(parts[3]);
        return new Product(id, product, productValue, productAmount);
    };

    private static final Function<Product, String> productToStringMapper = product ->
            product.getId() + "|" + product.getProduct() + "|" + product.getProductValue() + "|" + product.getProductAmount();


    @Override
    public Product ProductfindById(Integer id) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    //.allMatch(product -> product.getId() == id)
                    .filter(product -> product.getId() == id)
                    .min(Comparator.comparingInt(Product::getId))
                    .get();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);

        }
    }

    @Override
    public  List<Product> findAllByTitleLike (String title) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getProduct().contains(title))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
