import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите длинну массива: ");
        int arrayLength = scanner.nextInt();

        if (arrayLength>1) {
            int[] array = new int[arrayLength];

            for (int i = 0; i < array.length; i++){
                array[i] = scanner.nextInt();
            }

            int localMinSum = 0;

            //Проверка левой границы массива
            if (array[0] < array [1]) {
                localMinSum++;
            }

            //Проверка правой границы массива
            if (array[array.length - 2] > array[array.length - 1]) {
                localMinSum++;
            }

            for (int i = 1; i < array.length-1; i++){
                if ((array[i - 1] > array[i]) && (array[i] < array[i + 1])) {
                    localMinSum++;
                }
            }

            if (localMinSum > 0){
                System.out.println("Количество локальных минимумов = " + localMinSum);
            } else {
                System.out.println("Локальных минимумов не обнаружено");
            }

        } else {
            System.out.println("Массив должен содержать минимуи 2 элемента!");
        }

    }
}